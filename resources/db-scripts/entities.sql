-- -----------------------------------------------------
-- Schema game-app
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `game-app`;

CREATE SCHEMA `game-app`;
USE `game-app` ;


SET FOREIGN_KEY_CHECKS = 0;


-- -----------------------------------------------------
-- Table `game-app`.`black_list`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `game-app`.`black_list` (
    `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
    `comment` VARCHAR(255) DEFAULT NULL,
    `date_created` datetime(6) DEFAULT NULL,
    PRIMARY KEY (`id`))
    ENGINE=InnoDB
    AUTO_INCREMENT = 1;


-- -----------------------------------------------------
-- Table `game-app`.`game`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `game-app`.`game` (
    `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(255) DEFAULT NULL,
    `description` VARCHAR(255) DEFAULT NULL,
    PRIMARY KEY (`id`)
    )
    ENGINE=InnoDB
    AUTO_INCREMENT = 1;

-- -----------------------------------------------------
-- Table `game-app`.`user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `game-app`.`user` (
    `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
    `user_name` VARCHAR(255) DEFAULT NULL,
    `phone_number` VARCHAR(255) DEFAULT NULL,
    `identification` VARCHAR(255) DEFAULT NULL,
    `password` VARCHAR(255) DEFAULT NULL,
    `email` VARCHAR(255) DEFAULT NULL,
    `black_list_id` BIGINT(20) DEFAULT NULL,
    `active` BIT(1) DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `FK_BLACKLIST_idx` (`black_list_id`),

    CONSTRAINT `FK_BLACKLIST`
    FOREIGN KEY (`black_list_id`)
    REFERENCES `black_list` (`id`)
    )
    ENGINE=InnoDB
    AUTO_INCREMENT = 1;

-- -----------------------------------------------------
-- Table `game-app`.`game_user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `game-app`.`game_user` (
    `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
    `score` INT(11) DEFAULT NULL,
    `date_created` datetime(6) DEFAULT NULL,
    `user_id` BIGINT(20) NOT NULL,
    `game_id` BIGINT(20) NOT NULL,
    PRIMARY KEY (`id`),
    KEY `fk_user1` (`user_id`),
    CONSTRAINT `fk_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
    CONSTRAINT `fk_game` FOREIGN KEY (`game_id`) REFERENCES `game` (`id`)
    )
    ENGINE=InnoDB
    AUTO_INCREMENT = 1;


-- -----------------------------------------------------
-- Table `game-app`.`error_log`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `game-app`.`error_log` (
    `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
    `content` VARCHAR(255) DEFAULT NULL,
    `date_created` datetime(6) DEFAULT NULL,
    `user_id` BIGINT(20) NOT NULL,
    PRIMARY KEY (`id`),
    CONSTRAINT `fk_user2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`))
    ENGINE=InnoDB
    AUTO_INCREMENT = 1;

-- -----------------------------------------------------
-- Table `game-app`.`authorities`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `game-app`.`authorities` (
    `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
    `authority` VARCHAR(255) DEFAULT NULL,
    `user_id` BIGINT(20) NOT NULL,
    PRIMARY KEY (`id`),
    CONSTRAINT `fk_user3` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`))
    ENGINE=InnoDB
    AUTO_INCREMENT = 1;

-- -----------------------------------------------------
-- Table `game-app`.`multi_game`
-- -----------------------------------------------------

CREATE TABLE IF NOT EXISTS `game-app`.`multi_game` (
    `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
    `game_id` VARCHAR(255) DEFAULT NULL,
    `player1` BIGINT(20) DEFAULT NULL,
    `player2` BIGINT(20) DEFAULT NULL,
    `game_player1` BIGINT(20) DEFAULT NULL,
    `game_player2` BIGINT(20) DEFAULT NULL,
    `status` VARCHAR(255) DEFAULT NULL,
    `board` VARCHAR(255) DEFAULT NULL,
    `winner` VARCHAR(255) DEFAULT NULL,
    `date_created` datetime(6) DEFAULT NULL,
    PRIMARY KEY (`id`))
    ENGINE=InnoDB
    AUTO_INCREMENT = 1;


INSERT INTO `game-app`.`game` (`name`, `description`) VALUES ('Tic Tac Toe', 'Tic Tac Toe game for 2 players');
INSERT INTO `game-app`.`game` (`name`, `description`) VALUES ('Graduation Road Game', 'Graduation Road game for one player');
INSERT INTO `game-app`.`game` (`name`, `description`) VALUES ('Card Game','Card Game for one player');


SET FOREIGN_KEY_CHECKS = 1;
