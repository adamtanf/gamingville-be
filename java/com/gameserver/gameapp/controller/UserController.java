package com.gameserver.gameapp.controller;

import com.gameserver.gameapp.dao.UserRepository;
import com.gameserver.gameapp.dto.RegisterRequestDto;
import com.gameserver.gameapp.dto.UserDetailsRequest;
import com.gameserver.gameapp.dto.UserDetailsResponse;
import com.gameserver.gameapp.entity.User;
import com.gameserver.gameapp.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping("/user")
@CrossOrigin("http://localhost:4200")
public class UserController {

    @Autowired
    private UserRepository repository;

    @Autowired
    private UserService userService;

    @PostMapping("/getUserDetails")
    @PreAuthorize("hasAuthority('ROLE_USER')")
    public ResponseEntity<UserDetailsResponse> getUserDetails(@RequestBody UserDetailsRequest request){
        User user = this.repository.findByUserNameAndIdentification(request.getName(), request.getIdentification());
        if(user == null) {
            return ResponseEntity.badRequest().build();
        }

        UserDetailsResponse userDetailsResponse = new UserDetailsResponse(user);

        return ResponseEntity.ok(userDetailsResponse);
    }

    @PostMapping("/update-user")
    @PreAuthorize("hasAuthority('ROLE_USER')")
    public ResponseEntity<Void> register(@RequestBody RegisterRequestDto requestDto){
        this.userService.updateUser(requestDto);

        return ResponseEntity.ok().build();
    }

    @PostMapping("/unblock-user")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public ResponseEntity<Void> unBlockUser(@RequestBody UserDetailsRequest requestDto){
        this.userService.unBlockUser(requestDto);

        return ResponseEntity.ok().build();
    }

    @PostMapping("/block-user")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public ResponseEntity<Void> blockUser(@RequestBody UserDetailsRequest requestDto){
        this.userService.blockUser(requestDto);

        return ResponseEntity.ok().build();
    }

    @PostMapping("/admin")
    @PreAuthorize("hasAuthority('ROLE_USER')")
    public ResponseEntity<Boolean> isAdmin(@RequestBody UserDetailsRequest requestDto){
        boolean hasAdminRole = this.userService.hasAdminRole(requestDto);

        return ResponseEntity.ok(hasAdminRole);
    }
}
