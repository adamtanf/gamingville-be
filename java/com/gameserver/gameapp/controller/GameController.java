package com.gameserver.gameapp.controller;

import com.gameserver.gameapp.dto.ConnectRequest;
import com.gameserver.gameapp.dto.GameIdentifierDto;
import com.gameserver.gameapp.dto.UserDetailsRequest;
import com.gameserver.gameapp.dto.UserGameHistoryDto;
import com.gameserver.gameapp.entity.GameUser;
import com.gameserver.gameapp.entity.MultiGame;
import com.gameserver.gameapp.dto.TicTacToeStepDto;
import com.gameserver.gameapp.service.GameService;
import com.gameserver.gameapp.service.TicTacToeGameService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@Slf4j
@AllArgsConstructor
@RequestMapping("/game")
@CrossOrigin("http://localhost:4200")
public class GameController {

    private final TicTacToeGameService gameService;
    private final SimpMessagingTemplate simpMessagingTemplate;
    private final GameService service;

    @PostMapping("/register-game")
    public ResponseEntity<Void> registerSingleGamePlayer(@RequestBody GameIdentifierDto request) {

        this.service.registerGame(request);

        return ResponseEntity.ok().build();
    }

    @PostMapping("/create-game")
    public ResponseEntity<MultiGame> start(@RequestBody UserDetailsRequest request) {
        return ResponseEntity.ok(gameService.createGame(request));
    }

    @PostMapping("/connect/exist-game")
    public ResponseEntity<MultiGame> connectExistGame(@RequestBody ConnectRequest request) {
        return ResponseEntity.ok(gameService.connectToRandomGame(request));
    }

    @PostMapping("/gameplay")
    public ResponseEntity<MultiGame> gamePlay(@RequestBody TicTacToeStepDto request) {
        MultiGame game = gameService.gamePlay(request);
        simpMessagingTemplate.convertAndSend("/topic/game-progress/" + game.getGameId(), game);
        return ResponseEntity.ok(game);
    }

    @PostMapping("/player-one-turn")
    public ResponseEntity<Boolean> isToStartPlay(@RequestBody String gameId) {
        boolean isPlayer1ToPlay = this.gameService.isPlayer1Turn(gameId);

        return ResponseEntity.ok(isPlayer1ToPlay);
    }

    @PostMapping("/games-history")
    public ResponseEntity<List<UserGameHistoryDto>> getAllUserGames(@RequestBody UserDetailsRequest request) {

        List<GameUser> gameUsers = this.service.getAllGamesOfUser(request.getName(), request.getIdentification());
        List<UserGameHistoryDto> gameHistoryList = new ArrayList<>();
        gameUsers.forEach(it -> {
            gameHistoryList.add(new UserGameHistoryDto(it.getScore(), it.getGame().getName(), it.getDateCreated()));
        });

        return ResponseEntity.ok(gameHistoryList);
    }
}
