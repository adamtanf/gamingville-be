package com.gameserver.gameapp.controller;

import com.gameserver.gameapp.dto.AuthRequest;
import com.gameserver.gameapp.dto.AuthResponse;
import com.gameserver.gameapp.dto.RegisterRequestDto;
import com.gameserver.gameapp.dto.UserResponseDto;
import com.gameserver.gameapp.entity.User;
import com.gameserver.gameapp.service.ErrorLogService;
import com.gameserver.gameapp.service.UserService;
import com.gameserver.gameapp.util.JwtUtil;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@Slf4j
@AllArgsConstructor
public class WelcomeController {

    private JwtUtil jwtUtil;
    private AuthenticationManager authenticationManager;
    private UserService userService;
    private ErrorLogService errorLogService;
    private BCryptPasswordEncoder passwordEncoder;


    @PostMapping("/authenticate")
    public AuthResponse generateToken(@RequestBody AuthRequest authRequest) {

        try{
            authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(authRequest.getUserName(),authRequest.getPassword())
            );
        }catch(Exception ex){
            String msg = "invalid username/password for user: " + authRequest.getUserName();
            this.errorLogService.setErrorLog(msg, null);
            ex.printStackTrace();
            throw new RuntimeException(msg);
        }

        User user  = this.userService.findByUserName(authRequest.getUserName());
        if(!this.passwordEncoder.matches(authRequest.getPassword(), user.getPassword())) {
            throw new RuntimeException("invalid password");
        }
        if(user.getBlackList() != null) {
            String msg = "Block User is not allowed to generateToken. User Details: Identification = "
                    + user.getIdentification() + ", user name: " + user.getUserName();
            this.errorLogService.setErrorLog(msg, user);
            throw new RuntimeException(msg);
        }
        String token = jwtUtil.generateToken(authRequest.getUserName());
        AuthResponse response = new AuthResponse();
        response.setToken(token);
        List<String> roles = new ArrayList<>();
        user.getRoles().forEach(
                role -> roles.add(role.getAuthority())
        );
        UserResponseDto userResponse = new UserResponseDto(user.getUserName(), user.getIdentification(), roles);
        response.setUser(userResponse);

        return response;
    }

    @RequestMapping("/register")
    public ResponseEntity<Void> register(@RequestBody RegisterRequestDto requestDto){

        String userDetails = requestDto.getUserDetails();

        log.info("Start validation of user {}", userDetails);

        if(this.userService.isValidateUser(requestDto) == false) {
            String msg = "Failed validation of user " + userDetails;
            log.error("{}",msg);
            this.errorLogService.setErrorLog(msg, null);
            return ResponseEntity.badRequest().build();
        }

        log.info("Success validation of user {}", userDetails);

        this.userService.registerUser(requestDto);

        log.info("Success registration of user {}", userDetails);

        return ResponseEntity.ok().build();
    }

    @RequestMapping("/check-user-name")
    public ResponseEntity<Boolean> checkUserName(@RequestBody String userName){

        boolean isValidUserName = false;

        isValidUserName = this.userService.isValidUserName(userName);

        return ResponseEntity.ok(isValidUserName);
    }
}
