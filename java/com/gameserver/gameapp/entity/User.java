package com.gameserver.gameapp.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Entity
@Table(name="user")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "user_name")
    private String userName;

    @Column(name = "email")
    private String email;

    @Column(name = "identification")
    private String identification;

    @Column(name = "phone_number")
    private String phoneNumber;

    @OneToMany(mappedBy = "user")
    private List<GameUser> gameUser;

    @OneToOne(cascade = { CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REMOVE }, orphanRemoval = true)
    @JoinColumn(name = "black_list_id")
    private BlackList blackList;

    @OneToMany(mappedBy = "user")
    private List<ErrorLog> errorLog;

    @Column(name = "password")
    private String password;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL,mappedBy = "user")
    private List<Role> roles;

    @Column(name = "active")
    private boolean active;

    public String[] getRolesStringArray(){
        String[] tempArr = new String[this.roles.size()];
        for(int i = 0; i < this.roles.size(); i++){;
            tempArr[i] = this.roles.get(i).getName();
        }
        //String[] tempArr = {"ROLE_ADMIN"};
        return tempArr;
    }

    public String getRolesString(){
        String temp = "";
        for(int i = 0; i < this.roles.size(); i++){;
            temp += this.roles.get(i).getName();
        }
        //temp = "ROLE_ADMIN";
        return temp;
    }

    public void setRoles(String role) {
        if(this.roles== null || this.roles.isEmpty()){
            this.roles = new ArrayList<>();
        }

        Role tmpRole = new Role();
        tmpRole.setUser(this);
        tmpRole.setAuthority(role);
        this.roles.add(tmpRole);
    }

    public String getUserDetails(){

        return "User name: " + this.getUserName() + ", identification: " + this.getIdentification();
    }
}
