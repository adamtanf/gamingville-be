package com.gameserver.gameapp.entity;

import org.springframework.data.rest.core.config.Projection;

import java.util.Date;

@Projection(name="errorLogProjection", types = {ErrorLog.class})
public interface ErrorLogProjection {

    String getContent();

    Date getDateCreated();

    User getUser();
}
