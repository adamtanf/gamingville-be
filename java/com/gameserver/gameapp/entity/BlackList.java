package com.gameserver.gameapp.entity;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="black_list")
@Getter
@Setter
public class BlackList {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "comment")
    private String comment;

    @Column(name = "date_created")
    @CreationTimestamp
    private Date dateCreated;

    @OneToOne(mappedBy = "blackList")
    private User user;

}
