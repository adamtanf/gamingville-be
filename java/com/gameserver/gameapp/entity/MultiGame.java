package com.gameserver.gameapp.entity;

import com.gameserver.gameapp.common.GameStatus;
import com.gameserver.gameapp.common.TicToe;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="multi_game")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class MultiGame {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    Long id;

    @Column(name = "game_id")
    String gameId;

    @Column(name = "player1")
    Long player1;

    @Column(name = "player2")
    Long player2;

    @Column(name = "game_player1")
    Long gamePlayer1;

    @Column(name = "game_player2")
    Long gamePlayer2;

    @Column(name = "status")
    GameStatus status;

    @Column(name = "board")
    String board;

    @Column(name = "winner")
    TicToe winner;

    @Column(name = "date_created")
    @CreationTimestamp
    private Date dateCreated;

    public void setBoard(int[][] board) {
        Gson gson = new GsonBuilder().create();

        this.board = gson.toJson(board);
    }

    public int[][] getBoard() {
        Gson gson = new GsonBuilder().create();

        return gson.fromJson(this.board, int[][].class);
    }
}
