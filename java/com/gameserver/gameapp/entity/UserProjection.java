package com.gameserver.gameapp.entity;

import org.springframework.data.rest.core.config.Projection;

import java.util.List;

@Projection(name="userProjection", types = {User.class})
public interface UserProjection {

    String getUserName();

    String getIdentification();

    BlackList getBlackList();

    boolean getActive();

    List<Role> getRoles();

}
