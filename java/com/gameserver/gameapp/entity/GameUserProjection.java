package com.gameserver.gameapp.entity;

import org.springframework.data.rest.core.config.Projection;

import java.util.Date;

@Projection(name="gameUserProjection", types = {GameUser.class})
public interface GameUserProjection {

    int getScore();

    User getUser();

    Game getGame();

    Date getDateCreated();
}
