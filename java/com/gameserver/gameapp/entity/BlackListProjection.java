package com.gameserver.gameapp.entity;

import org.springframework.data.rest.core.config.Projection;

import java.util.Date;

@Projection(name="blackListProjection", types = {BlackList.class})
public interface BlackListProjection {

    String getComment();

    Date getDateCreated();

    User getUser();
}
