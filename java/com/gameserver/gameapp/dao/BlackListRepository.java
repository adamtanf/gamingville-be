package com.gameserver.gameapp.dao;

import com.gameserver.gameapp.entity.BlackList;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

@RepositoryRestResource
@CrossOrigin("http://localhost:4200")
public interface BlackListRepository extends JpaRepository<BlackList, Long> {

    @Modifying
    @Query("delete from BlackList b where b.id=:entityId")
    void delete(Long entityId);
}
