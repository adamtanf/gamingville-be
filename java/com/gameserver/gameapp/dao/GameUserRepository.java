package com.gameserver.gameapp.dao;

import com.gameserver.gameapp.entity.GameUser;
import com.gameserver.gameapp.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.List;

@RepositoryRestResource
@CrossOrigin("http://localhost:4200")
public interface GameUserRepository extends JpaRepository<GameUser, Long> {
    List<GameUser> findAllByUser(User user);
}
