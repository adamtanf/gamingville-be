package com.gameserver.gameapp.dao;

import com.gameserver.gameapp.common.GameStatus;
import com.gameserver.gameapp.entity.MultiGame;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.List;

@RepositoryRestResource
@CrossOrigin("http://localhost:4200")
public interface MultiGameRepository extends JpaRepository<MultiGame, Long> {

    MultiGame findMultiGameByGameId(String gameId);

    MultiGame findFirstByStatus(GameStatus status);

    MultiGame findByGameId(String gameId);
}
