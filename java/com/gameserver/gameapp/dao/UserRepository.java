package com.gameserver.gameapp.dao;

import com.gameserver.gameapp.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

@RepositoryRestResource
@CrossOrigin("http://localhost:4200")
public interface UserRepository extends JpaRepository<User, Long> {
    User findByUserName(String username);

    User findByUserNameAndPassword(String username, String password);

    User findByUserNameAndIdentification(String username, String identification);
}
