package com.gameserver.gameapp.service;

import com.gameserver.gameapp.dao.BlackListRepository;
import com.gameserver.gameapp.dao.UserRepository;
import com.gameserver.gameapp.dto.RegisterRequestDto;
import com.gameserver.gameapp.dto.UserDetailsRequest;
import com.gameserver.gameapp.entity.BlackList;
import com.gameserver.gameapp.entity.Role;
import com.gameserver.gameapp.entity.User;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Service
@Slf4j
@AllArgsConstructor
public class UserService {

    private UserRepository userRepository;
    private BlackListRepository blackListRepository;
    private BCryptPasswordEncoder passwordEncoder;
    private ErrorLogService errorLogService;

    public User findByUserNameAndPassword(String username, String password) {

        return this.userRepository.findByUserNameAndPassword(username, password);
    }

    public User findByUserNameAndIdentification(String username, String identification) {

        return this.userRepository.findByUserNameAndIdentification(username, identification);
    }

    public boolean isValidateUser(RegisterRequestDto requestDto) {

        if(this.userRepository.findByUserNameAndIdentification(requestDto.getUserName(),
                requestDto.getIdentification()) != null) {
            return false;
        }

        return true;
    }

    @Transactional
    public void registerUser(RegisterRequestDto requestDto) {
        User user = new User();
        user.setRoles("ROLE_USER");
        user.setActive(true);
        user.setUserName(requestDto.getUserName());
        user.setEmail(requestDto.getEmail());
        user.setPhoneNumber(requestDto.getPhoneNumber());
        user.setIdentification(requestDto.getIdentification());
        user.setPassword(this.passwordEncoder.encode(requestDto.getPassword()));
        this.userRepository.save(user);
    }

    @Transactional
    public void updateUser(RegisterRequestDto requestDto) {
        User user = this.userRepository.findByUserNameAndIdentification(requestDto.getPreviousUserName(), requestDto.getPreviousIdentification());
        if(user == null) {
            String msg = "User was not found";
            errorLogService.setErrorLog(msg, null);
            throw new RuntimeException(msg);
        }

        user.setUserName(requestDto.getUserName());
        user.setEmail(requestDto.getEmail());
        user.setPhoneNumber(requestDto.getPhoneNumber());
        user.setIdentification(requestDto.getIdentification());

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null && auth.getAuthorities().stream().anyMatch(a -> a.getAuthority().equals("ADMIN"))) {
            if(requestDto.getRole() != null) {
                boolean hasRole = false;
                for(Role role : user.getRoles()) {
                    if(role.getAuthority() == requestDto.getRole()){
                        hasRole = true;
                    }
                }
                if(hasRole == false) {
                    user.setRoles(requestDto.getRole());
                }
            }
        }

        this.userRepository.save(user);
    }

    @Transactional
    public void unBlockUser(UserDetailsRequest requestDto) {
        User user = this.userRepository.findByUserNameAndIdentification(requestDto.getName(), requestDto.getIdentification());
        if(user == null) {
            String msg = "User was not found";
            errorLogService.setErrorLog(msg, null);
            throw new RuntimeException(msg);
        }

        long id = user.getBlackList().getId();
        user.setBlackList(null);
        this.userRepository.save(user);
        this.blackListRepository.delete(id);
    }

    @Transactional
    public void blockUser(UserDetailsRequest requestDto) {
        User user = this.userRepository.findByUserNameAndIdentification(requestDto.getName(), requestDto.getIdentification());
        User authenticateUser = this.getAuthenticateUser();
        if(user == null) {
            String msg = "Blocking user failed. user was not found";
            errorLogService.setErrorLog(msg, authenticateUser);
            throw new RuntimeException(msg);
        }

        BlackList blackList = new BlackList();
        blackList.setUser(user);
        blackList.setDateCreated(new Date());
        blackList.setComment("User was blocked by user: " + authenticateUser.getUserName());
        this.blackListRepository.save(blackList);
        user.setBlackList(blackList);
        this.userRepository.save(user);
    }

    public boolean hasAdminRole(UserDetailsRequest requestDto) {
        User user = this.userRepository.findByUserNameAndIdentification(requestDto.getName(), requestDto.getIdentification());
        if(user == null) {
            String msg = "User was not found";
            errorLogService.setErrorLog(msg, null);
            throw new RuntimeException(msg);
        }

        boolean hasAdmin = false;

        List<Role> userRoles = user.getRoles();
        for(Role role  :  userRoles) {
            if(role.getAuthority().equals("ROLE_ADMIN")) {
                hasAdmin = true;
            }
        }

        return hasAdmin;
    }

    public boolean isValidUserName(String userName){
        User user = this.userRepository.findByUserName(userName);

        if(user != null){
            return false;
        }

        return true;
    }

    public User findByUserName(String userName) {
        return this.userRepository.findByUserName(userName);
    }

    public User getAuthenticateUser(){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        org.springframework.security.core.userdetails.UserDetails pricipal = (UserDetails) auth.getPrincipal();

        return this.findByUserNameAndPassword(pricipal.getUsername(), pricipal.getPassword());
    }
}
