package com.gameserver.gameapp.service;

import com.gameserver.gameapp.dao.UserRepository;
import com.gameserver.gameapp.entity.User;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@Slf4j
@AllArgsConstructor
public class CustomUserDetailsService implements UserDetailsService {

    private UserRepository repository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        User user = repository.findByUserName(username);

        log.info("Hello user: " + user.getUserName());

        Optional<User> tempUser = Optional.of(user);

        return tempUser.map(com.gameserver.gameapp.service.UserDetails::new).orElseThrow(()->new UsernameNotFoundException(username + " Not Found")) ;
    }
}
