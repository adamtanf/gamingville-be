package com.gameserver.gameapp.service;

import com.gameserver.gameapp.dao.GameRepository;
import com.gameserver.gameapp.dao.GameUserRepository;
import com.gameserver.gameapp.dto.GameIdentifierDto;
import com.gameserver.gameapp.entity.Game;
import com.gameserver.gameapp.entity.GameUser;
import com.gameserver.gameapp.entity.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@Service
public class GameService {

    private GameUserRepository gameUserRepository;
    private GameRepository gameRepository;
    private UserService userService;

    public void saveSinglePlayerGame(GameUser gameUser) {

        this.gameUserRepository.save(gameUser);
    }

    public Game findGameById(long id){

        return this.gameRepository.getById(id);
    }

    public void registerGame(GameIdentifierDto request) {
        User user = userService.findByUserNameAndIdentification(request.getName(), request.getIdentification());
        Game game = this.findGameById(Long.parseLong(request.getGameId()));
        GameUser gameUser = new GameUser();
        gameUser.setUser(user);
        gameUser.setGame(game);
        gameUser.setDateCreated(new Date());
        gameUser.setScore(request.getScore());

        this.saveSinglePlayerGame(gameUser);
    }

    public List<GameUser> getAllGamesOfUser(String name, String identification) {
        User user = userService.findByUserNameAndIdentification(name, identification);

        return this.gameUserRepository.findAllByUser(user);
    }
}
