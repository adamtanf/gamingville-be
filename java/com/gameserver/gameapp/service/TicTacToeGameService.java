package com.gameserver.gameapp.service;

import com.gameserver.gameapp.dao.GameRepository;
import com.gameserver.gameapp.dao.GameUserRepository;
import com.gameserver.gameapp.dao.MultiGameRepository;
import com.gameserver.gameapp.dao.UserRepository;
import com.gameserver.gameapp.dto.ConnectRequest;
import com.gameserver.gameapp.dto.UserDetailsRequest;
import com.gameserver.gameapp.entity.GameUser;
import com.gameserver.gameapp.entity.MultiGame;
import com.gameserver.gameapp.entity.User;
import com.gameserver.gameapp.dto.TicTacToeStepDto;
import com.gameserver.gameapp.common.TicToe;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.Date;
import java.util.UUID;

import static com.gameserver.gameapp.common.GameStatus.*;


@Service
public class TicTacToeGameService {

    private MultiGameRepository multiGameRepository;
    private UserRepository userRepository;
    private GameRepository gameRepository;
    private GameUserRepository gameUserRepository;
    private ErrorLogService errorLogService;

    @Value("${multi.game.id}")
    private Long multiGameId;

    @Autowired
    public TicTacToeGameService(MultiGameRepository multiGameRepository, UserRepository userRepository,
                                GameRepository gameRepository, GameUserRepository gameUserRepository,
                                ErrorLogService errorLogService) {
        this.multiGameRepository = multiGameRepository;
        this.userRepository = userRepository;
        this.gameRepository = gameRepository;
        this.gameUserRepository = gameUserRepository;
        this.errorLogService = errorLogService;
    }

    @Transactional
    public MultiGame createGame(UserDetailsRequest request) {
        User user = this.userRepository.findByUserNameAndIdentification(request.getName(), request.getIdentification());
        MultiGame game = new MultiGame();
        game.setBoard(new int[3][3]);
        game.setGameId(UUID.randomUUID().toString());
        game.setPlayer1(user.getId());
        game.setStatus(NEW);
        game.setDateCreated(new Date());
        GameUser gameUser = new GameUser();
        gameUser.setGame(this.gameRepository.getById(this.multiGameId));
        gameUser.setUser(user);
        gameUser.setDateCreated(new Date());
        this.gameUserRepository.save(gameUser);
        game.setGamePlayer1(gameUser.getId());
        this.multiGameRepository.save(game);
        return game;
    }

    @Transactional
    public MultiGame connectToRandomGame(ConnectRequest request) {

        User user = this.userRepository.findByUserNameAndIdentification(request.getName(), request.getIdentification());
        MultiGame game = this.multiGameRepository.findFirstByStatus(NEW);

        if(game == null){
            String msg = "No available games were found for user: " + user.getUserDetails();
            this.errorLogService.setErrorLog(msg, null);
            throw new RuntimeException("Game not found");
        }

        GameUser gameUser = new GameUser();
        gameUser.setGame(this.gameRepository.getById(this.multiGameId));
        gameUser.setUser(user);
        gameUser.setDateCreated(new Date());
        this.gameUserRepository.save(gameUser);
        game.setGamePlayer2(gameUser.getId());

        game.setPlayer2(user.getId());
        game.setStatus(IN_PROGRESS);

        this.multiGameRepository.save(game);
        return game;
    }

    @Transactional
    public MultiGame gamePlay(TicTacToeStepDto gamePlay) {
        MultiGame game = this.multiGameRepository.findMultiGameByGameId(gamePlay.getGameId());
        this.errorLogService.setErrorLog("hi", null);
        if (game == null) {
            String msg = "No game were found with id: " + gamePlay.getGameId();
            this.errorLogService.setErrorLog(msg, null);
            throw new RuntimeException("Game not found");
        }

        if (game.getStatus().equals(FINISHED)) {
            String msg = "Game with id: " + gamePlay.getGameId() + " was FINISHED";
            this.errorLogService.setErrorLog(msg, null);
            throw new RuntimeException(msg);
        }

        int[][] board = game.getBoard();
        board[gamePlay.getCoordinateX()][gamePlay.getCoordinateY()] = gamePlay.getType().getValue();

        Boolean xWinner = checkWinner(game.getBoard(), TicToe.X);
        Boolean oWinner = checkWinner(game.getBoard(), TicToe.O);
        if (xWinner) {
            this.updateGameUser(game.getGamePlayer1(), 100);
            this.updateGameUser(game.getGamePlayer2(), 0);
            game.setWinner(TicToe.X);
        } else if (oWinner) {
            this.updateGameUser(game.getGamePlayer1(), 0);
            this.updateGameUser(game.getGamePlayer2(), 100);
            game.setWinner(TicToe.O);
        }

        game.setBoard(board);
        this.multiGameRepository.save(game);
        return game;
    }

    @Transactional
    public void updateGameUser(Long gameUserId, int score) {
        GameUser gameUser = this.gameUserRepository.getById(gameUserId);
        gameUser.setScore(score);
        this.gameUserRepository.save(gameUser);
    }

    private Boolean checkWinner(int[][] board, TicToe ticToe) {
        int[] boardArray = new int[9];
        int counterIndex = 0;
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                boardArray[counterIndex] = board[i][j];
                counterIndex++;
            }
        }

        int[][] winCombinations = {{0, 1, 2}, {3, 4, 5}, {6, 7, 8}, {0, 3, 6}, {1, 4, 7}, {2, 5, 8}, {0, 4, 8}, {2, 4, 6}};
        for (int i = 0; i < winCombinations.length; i++) {
            int counter = 0;
            for (int j = 0; j < winCombinations[i].length; j++) {
                if (boardArray[winCombinations[i][j]] == ticToe.getValue()) {
                    counter++;
                    if (counter == 3) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public boolean isPlayer1Turn(String gameId) {

        MultiGame game = this.multiGameRepository.findByGameId(gameId);
        if(game == null) {
            return false;
        }

        return game.getGamePlayer2() != null;
    }
}
