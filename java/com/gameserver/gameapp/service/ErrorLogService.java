package com.gameserver.gameapp.service;

import com.gameserver.gameapp.dao.ErrorLogRepository;
import com.gameserver.gameapp.dao.UserRepository;
import com.gameserver.gameapp.entity.ErrorLog;
import com.gameserver.gameapp.entity.User;
import lombok.AllArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

@Service
@AllArgsConstructor
public class ErrorLogService {

    private ErrorLogRepository errorLogRepository;
    private UserRepository userRepository;

    @Transactional
    public void setErrorLog(String msg, User user) {

        User currentUser = user;
        if(currentUser == null) {
            user = this.getAuthenticateUser();
        }

        ErrorLog errorLog = new ErrorLog();
        errorLog.setDateCreated(new Date());
        errorLog.setContent(msg);
        if(user != null){
            errorLog.setUser(user);
        }

        this.errorLogRepository.save(errorLog);
    }

    public User getAuthenticateUser(){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        org.springframework.security.core.userdetails.UserDetails pricipal = (UserDetails) auth.getPrincipal();

        return this.userRepository.findByUserNameAndPassword(pricipal.getUsername(), pricipal.getPassword());
    }
}
