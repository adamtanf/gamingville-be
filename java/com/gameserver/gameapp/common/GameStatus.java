package com.gameserver.gameapp.common;

public enum GameStatus {
    NEW, IN_PROGRESS, FINISHED
}
