package com.gameserver.gameapp.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GameIdentifierDto {

    String gameId;
    String name;
    String identification;
    Integer score;
}
