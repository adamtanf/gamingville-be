package com.gameserver.gameapp.dto;

import lombok.Data;

@Data
public class ConnectRequest {
    private String name;
    private String identification;
    private String gameId;
}
