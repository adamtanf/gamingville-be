package com.gameserver.gameapp.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Date;

@Data
@AllArgsConstructor
public class UserGameHistoryDto {

    int score;
    String gameName;
    Date date;
}
