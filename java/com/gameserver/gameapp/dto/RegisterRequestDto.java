package com.gameserver.gameapp.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RegisterRequestDto {

    String userName;

    String email;

    String identification;

    String phoneNumber;

    String password;

    String role;

    String previousUserName;

    String previousIdentification;

    public String getUserDetails() {

        return "User name: " + userName + "identification: " + identification + ", email: " + email + ", phoneNumber: "
                + phoneNumber;
    }
}
