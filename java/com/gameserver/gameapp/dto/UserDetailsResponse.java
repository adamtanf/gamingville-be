package com.gameserver.gameapp.dto;

import com.gameserver.gameapp.entity.User;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class UserDetailsResponse {

    String userName;
    String email;
    String identification;
    String phoneNumber;
    String password;

    public UserDetailsResponse(User user){
        this.userName = user.getUserName();
        this.password = user.getPassword();
        this.email = user.getEmail();
        this.identification = user.getIdentification();
        this.phoneNumber = user.getPhoneNumber();
    }
}
