package com.gameserver.gameapp.dto;

import com.gameserver.gameapp.common.TicToe;
import lombok.Data;

@Data
public class TicTacToeStepDto {

    private TicToe type;
    private Integer coordinateX;
    private Integer coordinateY;
    private String gameId;
}
